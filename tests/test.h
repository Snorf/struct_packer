
#pragma once
#include <stdlib.h>

typedef enum {
    NONE,
    EMPTY,
    //comment inside
    THIRD,
} type_e;

typedef int32_t ullong;

// This is a single-line comment
/* this is also a single-line comment with a block comment format */

   // This is a single-line comment with spaces in front
/* Multi
 * line
 * block
 *
 * comment */
typedef struct simple {
    int32_t content;
} simple;

typedef struct field {
    char   field_name[128];
    char   matrix[128][256];
    type_e type;
    size_t size;
    long long double_name;
    int32_t random;
    void *ptr;
    void* ptr2;
    char * * * triple_ptr;
    char potential_padd[2];
} field_t;

