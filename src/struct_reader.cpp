
#include <algorithm>
#include <fstream>
#include <iostream>
#include <format>
#include <ranges>
#include <optional>
#include <string>
#include <string_view>

#include <stdint.h>
#include <type_traits>
#include <utility>

#include "struct_reader.h"

enum class State {
    EMPTY,
    COMMENT,
    PRE_STRUCT, 
    IN_STRUCT, 
    POST_STRUCT, 
    PRE_ENUM, 
    IN_ENUM, 
    POST_ENUM, 
};

enum class InnerState {
    TYPE_DECL,
    NAME,
    NONE,
};

enum class Comment_type {
    SINGLE,
    BLOCK_INIT,
    BLOCK_CLOSE
};

//Nasty, but it does the trick
std::pair<size_t, size_t> _get_size(type_e type)
{
    switch(type) {
        case type_e::CHAR8:
            return std::make_pair(std::alignment_of<char>::value, sizeof(char));
        case type_e::SHORT:
            return std::make_pair(std::alignment_of<short>::value, sizeof(short));
        case type_e::SHORTINT:
            return std::make_pair(std::alignment_of<short int>::value, sizeof(short int));
        case type_e::INT:
            return std::make_pair(std::alignment_of<int>::value, sizeof(int));
        case type_e::INT8:
            return std::make_pair(std::alignment_of<int8_t>::value, sizeof(int8_t));
        case type_e::INT16:
            return std::make_pair(std::alignment_of<int16_t>::value, sizeof(int16_t));
        case type_e::INT32:
            return std::make_pair(std::alignment_of<int32_t>::value, sizeof(int32_t));
        case type_e::INT64:
            return std::make_pair(std::alignment_of<int64_t>::value, sizeof(int64_t));
        case type_e::LONG:
            return std::make_pair(std::alignment_of<long>::value, sizeof(long));
        case type_e::LONGINT:
            return std::make_pair(std::alignment_of<long int>::value, sizeof(long int));
        case type_e::LONGLONG:
            return std::make_pair(std::alignment_of<long long>::value, sizeof(long long));
        case type_e::LONGDOUBLE:
            return std::make_pair(std::alignment_of<long double>::value, sizeof(long double));
        case type_e::FLOAT32:
            return std::make_pair(std::alignment_of<float>::value, sizeof(float));
        case type_e::FLOAT64:
            return std::make_pair(std::alignment_of<double>::value, sizeof(double));
        case type_e::PTR:
            return std::make_pair(std::alignment_of<char *>::value, sizeof(char *));
        case type_e::NONE:
            return std::make_pair(0, 0);
        default:
            std::cout << "Struct/Enum size parsing not implemented!\n";
            return std::make_pair(0, 0);
    }
}

type_e _get_type(std::string_view type_str)
{
    if (type_str.find("*")!= std::string::npos) {
        return type_e::PTR;
    }
    if (type_str == "char" || type_str == "int8_" || type_str == "uint8_t") {
        return type_e::CHAR8;
    }
    if (type_str == "short") {
        return type_e::SHORT;
    }
    if (type_str == "int16_t" || type_str == "uint16_t") {
        return type_e::INT16;
    }
    if (type_str == "int32_t" || type_str == "uint32_t") {
        return type_e::INT32;
    }
    if (type_str == "int") {
        return type_e::INT;
    }
    if (type_str == "long") {
        return type_e::LONG;
    }
    if (type_str == "float") {
        return type_e::FLOAT32;
    }
    if (type_str == "double") {
        return type_e::FLOAT64;
    }
    if (type_str == "size_t") {
        return type_e::INT64;
    }
    if (type_str == "void") { //void must be always a pointer
        return type_e::PTR;
    }
    return type_e::NONE;
}

std::optional<Comment_type> _contains_comment(std::string_view line)
{
    int32_t match = 0;
    for (auto c : line) {
        if (c == ' ') continue;
        if (c == '/') {
            match++;
            if (match == 2) return Comment_type::SINGLE;
            continue;
        }
        if (c == '*' && match) { //begins with a comment
            match++;
            break;
        }
        else {
            break;
        }
    }

    int32_t second_match = 0;
    for (auto c = line.rbegin(); c != line.rend(); ++c) {
        if (*c == ' ') continue;

        if (*c == '/') second_match++;

        if (*c == '*' && second_match) { //ends a comment block
            second_match++;
            break;
        }
    }

    if (match == 2 && second_match == 2) {
        return Comment_type::SINGLE;
    }

    if (match == 2) { //multi line comment but not closed
        return Comment_type::BLOCK_INIT;
    }

    if (second_match == 2) {
        return Comment_type::BLOCK_CLOSE;
    }
    return std::nullopt;
}

bool _is_pragma(std::string_view line)
{
    if (line.starts_with("#pragma")) return true;
    return false;
}

type_e _get_new_type(type_e old_type, type_e incoming_type)
{
    //the only double word types that exist in C are:
    //short int -> irrelevant because sizeof(short) == sizeof(short int)
    //long int -> stays as long
    //long long -> promotes to int64_t
    //long double [*]
    switch(old_type) {
        case type_e::SHORT:
            if (incoming_type == type_e::INT) {
                return type_e::SHORTINT;
            }
            break;
        case type_e::LONG:
            if (incoming_type == type_e::INT) {
                return  type_e::LONGINT;
            }
            else if (incoming_type == type_e::LONG) {
                return type_e::LONGLONG;
            }
            else if (incoming_type == type_e::FLOAT64) {
                return type_e::LONGDOUBLE;
            }
            break;
        case type_e::PTR:
            if (incoming_type == type_e::PTR) {
                return type_e::PTR;
            }
            break;
        case type_e::NONE:
            return incoming_type;
        default:
            break;
    }
    return old_type;
}

void Struct_file::generate_structs(const char *file_name)
{
    std::ifstream ifs { file_name };     
    if (!ifs) {
        std::cerr << "Could not open " << file_name << " for reading\n"; return;
    }

    std::string line;
    State current_state = State::EMPTY;
    struct_s tmp_struct = { };
    enum_s   tmp_enum = { };

    while (std::getline(ifs, line)) {
        // empty lines and pragmas can be ignored
        if (line.empty()) continue;    
        if (_is_pragma(line)) continue;

        //comments can be ignored
        auto containts_comment = _contains_comment(line);
        if (containts_comment) {
            if (containts_comment.value() == Comment_type::BLOCK_INIT) {
                current_state = State::COMMENT;
            }
            else if (containts_comment.value() == Comment_type::BLOCK_CLOSE) {
                current_state = State::EMPTY; //should we continue here??
            }
            continue;
        }

        if (current_state == State::COMMENT) continue;

        auto inner_state = InnerState::TYPE_DECL;

        //std::cout << "read line " << line << "\n";

        // only declarations should be left here
        auto      separator = ' ';
        field     tmp_field = {};
        typedef_s tmp_typedef= {};
        bool      will_type_def = false;
        for (auto word : std::views::split(line, separator)) {
            auto re_word = std::string_view(word);
            if (re_word.size() < 1) continue; //skip extra spaces

            if (current_state == State::EMPTY) {
                if (re_word == "typedef") {
                    will_type_def = true;
                    continue;
                }
                if (re_word == "struct") {
                    current_state = State::PRE_STRUCT; 
                    continue;
                }
                if (re_word == "enum") {
                    //current_state = State::PRE_ENUM; 
                    will_type_def = false; //for now, just to be sure
                    continue;
                }
                //this is a typedef/define
                if (will_type_def) {
                    if (tmp_typedef.aliased_value.length() > 1) {
                        auto new_type = _get_type(re_word);
                        if (new_type != type_e::NONE) {

                        }
                        tmp_typedef.typedef_name = re_word;
                        tmp_typedef.typedef_name.erase(std::remove_if(tmp_typedef.typedef_name.begin(), 
                                    tmp_typedef.typedef_name.end(),
                                    [](const char &a) {return a==';';}), tmp_typedef.typedef_name.end());
                        this->typedefs.push_back(tmp_typedef);
                        tmp_typedef = {};
                        continue;
                    } else {
                        tmp_typedef.aliased_value = re_word;
                    }
                }

            }
            if (current_state == State::PRE_STRUCT) {
                if (re_word == "{") {
                    current_state = State::IN_STRUCT;
                    continue;
                }
                tmp_struct.name = re_word;
            }

            if (current_state == State::IN_STRUCT) {
                if (re_word == "}") {
                    current_state = State::POST_STRUCT;
                    continue;
                }
                if (inner_state == InnerState::TYPE_DECL) {
                    if (re_word == "unsigned") continue; //signedness does not matter for this
                    if (re_word == "signed") continue; //signedness does not matter for this

                    if (tmp_field.type != type_e::NONE) {
                        auto field_type = _get_type(re_word);
                        if (field_type != type_e::NONE) {
                            tmp_field.type = _get_new_type(tmp_field.type, field_type);
                            std::tie(tmp_field.alignment, tmp_field.size) = _get_size(tmp_field.type);
                            //if the new type is a PTR, it might be something like "*pointer", which is both type and name
                            //size == 1 could be something freaky like the middle "*" in "char * * *pointer"
                            if (tmp_field.type == type_e::PTR && re_word.size() > 1) {
                                inner_state = InnerState::NAME; 
                            }
                        } else {
                            inner_state = InnerState::NAME;
                        }
                    } else {
                        tmp_field.type = _get_type(re_word);
                        std::tie(tmp_field.alignment, tmp_field.size) = _get_size(tmp_field.type);
                        continue;
                    }
                }

                if (inner_state == InnerState::NAME) {
                    tmp_field.name = re_word;
                    tmp_field.name.erase(std::remove_if(tmp_field.name.begin(), tmp_field.name.end(), 
                                [](const char &a) {return (a == ';' || a == '*');}), tmp_field.name.end());
                    tmp_struct.fields.push_back(tmp_field);
                    inner_state = InnerState::NONE;
                    continue;
                }
            }

            if (current_state == State::POST_STRUCT) {
                if (re_word.contains(";")) {
                    if (will_type_def) {
                        tmp_struct.typedef_name = re_word;
                        tmp_struct.typedef_name.erase(std::remove_if(tmp_struct.typedef_name.begin(), 
                                    tmp_struct.typedef_name.end(),
                                    [](const char &a) {return a==';';}), tmp_struct.typedef_name.end());
                        will_type_def = false;
                    }
                    current_state = State::EMPTY;
                    this->structs.push_back(tmp_struct);
                    tmp_struct = {};
                    continue;
                }
            }
        }
    }
}

void Struct_file::fix_sizes()
{
    for (auto &strct : this->structs) {
        for (auto &field : strct.fields) {
            size_t first_pos = 0;
            size_t start_pos = 0;
            size_t end_pos = 0;
            while (((start_pos = field.name.find("["), end_pos)    != std::string::npos) && 
                    ((end_pos   = field.name.find("]",  end_pos+1)) != std::string::npos)) { 

                //move ahead the starting position after the [
                ++start_pos;
                if (first_pos == 0) first_pos = start_pos; //save the position of the first [
                std::string num_str = {&field.name[start_pos], end_pos - start_pos };
                field.size *= std::stol(num_str);
            }
            //if it is an array, remove the notation from the name
            if (first_pos != 0) {
                field.name = std::string(field.name, 0, start_pos);
            }

            //if it's a pointer, we change the type to pointer
            if (field.name.find("*") != std::string::npos) {
                field.type = type_e::PTR;
                std::tie(field.alignment, field.size) = _get_size(field.type);
            }

        }
    }
}


void Struct_file::find_optimal_order()
{
    for (auto &strct : this->structs) {
        //usually sorting by alignment size can be enough
        std::sort(strct.fields.begin(), strct.fields.end(), 
                [](field a, field b) {
                if (a.alignment == b.alignment) return a.size > b.size;
                return a.alignment > b.alignment; }   );

        for (auto const& [idx, field] : strct.fields | std::views::enumerate) {
            auto padd = strct.size % field.alignment;
            if (padd) {
                //convert the padding to its actual value
                padd = field.alignment - padd;

                //std::cout << "There will be padding before " << field.name << " with size " << padd << "\n";
                struct field potential = {};
                bool change = false; //if we have to change
                auto aux_idx = idx + 1;

                //For each remaining field (the previous are bigger so it would not fit), check if we can reduce the padding
                for (auto it = strct.fields.begin()+idx+1; it != strct.fields.end(); ++it) {
                    auto aux_field = *it;
                    //this is the padding for the potential field
                    auto new_padd = strct.size % aux_field.alignment;
                    //convert it to actual value
                    if (new_padd) new_padd = aux_field.alignment - new_padd;

                    //already a better fit (no padding), we can try to see if this helps
                    if (new_padd == 0) { 
                        auto potential_padd = (strct.size + aux_field.size) % field.alignment;
                        //this makes this a perfect candidate since it fits perfectly
                        if (potential_padd == 0) { 
                            potential = aux_field;
                            change = true;
                            break;
                        }
                        //this makes an acceptable candidate (we re-convert the potential padding)
                        else if ((field.alignment - potential_padd) < padd) { 
                            potential = aux_field;
                            change = true;
                        }
                    }
                    //we have less padding, so it's better but not ideal
                    else if (new_padd < padd) {
                        auto potential_padd = (strct.size + aux_field.size) % field.alignment;
                        //this makes it a decent candidate, since the new padding is better than the current one
                        if ((field.alignment - potential_padd) < padd) { 
                            potential = aux_field;
                            change = true;
                        }
                    }
                    aux_idx++;
                }
                if (change) {
                    strct.fields.erase(strct.fields.begin() + aux_idx);
                    strct.fields.emplace(strct.fields.begin() + idx, potential);
                }
                else {
                    strct.size += padd;
                }
            }
            strct.size += field.size;
        }
    }
}

Struct_file::Struct_file(const char *file_name)
{

    this->generate_structs(file_name);
    this->fix_sizes();

    if (this->typedefs.size() > 0) {
        std::cout << "  -------------------------------------------\n";
        std::cout << "  |      Printing non-struct typedefs!      |\n";
        std::cout << "  -------------------------------------------\n\n";
    }

    for (auto &typdf : this->typedefs) {
        std::cout << std::format("  {} = {} \n", typdf.typedef_name, typdf.aliased_value);
    }

    std::cout << "\n\n";
    std::cout << "  --------------------------------------------\n";
    std::cout << "  | Printing all the structs and its values! |\n";
    std::cout << "  --------------------------------------------\n";

    this->find_optimal_order();
    for (auto &strct : this->structs) {
        if (strct.typedef_name.length() > 0) {
            std::cout << std::format("\n  Struct {} (typedef'd as {}) with size {}\n", 
                    strct.name, strct.typedef_name, strct.size);
        } else {
            std::cout << std::format("\n  Struct {} with size {}\n", strct.name, strct.size);
        }
        for (auto &field : strct.fields) {
            std::cout << std::format("\t  |{:16}| |size {:8}| |alignment {:3}| \n", 
                    field.name, field.size, field.alignment);
        }
    }
    return;
}

