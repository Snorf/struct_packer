#include "struct_reader.h"

#include <iostream>

int32_t usage()
{
    std::cout << "Usage: struct_packer [file_with_struct.h]\n";
    return 0;
}

int32_t main(int32_t argc, const char *argv[])
{
    if (argc < 2) return usage();

    Struct_file s_file = Struct_file(argv[1]);

    return 0;
}
