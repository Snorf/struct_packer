
# Struct_packer
A simple program to determine the best order for struct members (to minimize padding).
As it is right now, it optimizes for minimal padding without caring about cache accesses nor
struct legibility.

Usage: './struct_packer [header.h]'

Next extensions:
    - print struct alignment
    - incorporate defines
    - typedef'd types
    - nested structs
    - incorporate includes
    - add an option to visualise cache lines when reading a struct
    - new strategies (not optimized for size)
    - C++ classes (?)

Limitations:
    - all pointers are assumed to be sizeof(char *)
    - the parser is a bit finnicky, so anything unusual may break it


The intended struct style is something along the lines of:

```
typedef struct example {
    char     *ptr;
    char*     also_a_ptr;
    int32_t   value;
    float     other_value;
    int64_t   matrix[128][128];
    long long array[128];
} example;
```


Other styles may not work.

