#pragma once

#include <vector>
#include <string>

/* Note that signedness does not matter (for size)
 * Also:
 *  SHORT, INT, and LONG are usually platform dependant, while the rest (stdint, floats) are not */
enum class type_e {
    NONE,
    CHAR8,
    SHORT,
    SHORTINT,
    INT, 
    INT8, 
    INT16,
    INT32,
    INT64,
    LONG, 
    LONGINT, 
    LONGLONG, 
    LONGDOUBLE, 
    FLOAT32,
    FLOAT64,
    PTR,
    STRUCT,
    ENUM,
};

typedef struct field {
    std::string name;
    type_e      type;
    size_t      size;
    size_t      alignment;
} field;

typedef struct struct_s {
    std::vector<field> fields;
    std::string        name;
    std::string        typedef_name;
    size_t             size;
} struct_s;

typedef struct typedef_s {
    std::string typedef_name;
    std::string aliased_value;
} typedef_s;

typedef struct enum_s {
    std::string name;
    int32_t     num_members;
    std::string typedef_name;
} enum_s;

class Struct_file {
    public:
        Struct_file(const char *file_name);
    private:
        void generate_structs(const char *file_name);
        void fix_sizes();
        void find_optimal_order();
        std::vector<struct_s> structs;
        std::vector<enum_s> enums;
        std::vector<typedef_s> typedefs;
};

